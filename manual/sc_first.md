## **1 Scenario Description**
The gift giving process under the 1 Scenario provides for:

1. users that will act as a Giver and a Presentee are Developer’s app users;
2. the availability of the following information at the moment of the initialization of **1 Scenario** on the side of the Developer’s app:
	- the country of residence of users who act as a Giver and a Presentee;
	- the city of residence of users who act as a Giver and a Presentee.
3. the data transfer from p.2 to YouGiver when you initialize the **Scenario 1**.

### **Scenario 1 initialization**
1. Send POST request to `https://service.yougiver.me/api/v1/gift_requests` with possible parameters:


<!--table-->

| Параметр                              	| Описание                                	|
|---------------------------------------	|------------------------------------------	|
| gift_request[developer_number] **\*** 	| Your developer_number in YouGiver service  	|
| gift_request[giver_id] **\***         	| The Giver ID in your service       	|
| gift_request[giver_name]              	| Name of the Giver                             	|
| gift_request[giver_email]             	| Email of the Giver                           	|
| gift_request[giver_phone]             	| Giver's phone number                         	|
| gift_request[giver_nickname]          	| Nickname of the Giver                         	|
| gift_request[giver_gender]            	| Gender of the Giver                             	|
| gift_request[recipient_id] **\***      	| The Presentee ID in your service   	|
| gift_request[recipient_name]          	| Presentee's name                           	|
| gift_request[recipient_email]         	| Presentee's email                         	|
| gift_request[recipient_country] **\*** 	| Country of the Presentee                        	|
| gift_request[recipient_city] **\***    	| City of the Presentee                         	|
| gift_request[recipient_address]       	| Presentee's address                         	|
| gift_request[recipient_gender]        	| Gender of the Presentee                           	|
| gift_request[recipient_nickname]      	| Presentee's Nickname                       	|

<!--endtable-->

\* required parameter.

1. The answer format will be in JSON:

<!--table-->

| Paremeter | Description                                     |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow |

<!--endtable-->

### **Paremeter Description**

  **All parameters are simple strings**

  - `gift_request[recipient_country]` — The country must be serviced by YouGiver.
  - `gift_request[recipient_city]` — The city must be serviced by YouGiver.

**For a list of the countries and cities YouGiver supports, see the ["Locations API"](https://yougiver.readthedocs.io/ru/latest/locations_api/) section.**

### **Supported event notifications in 1 Scenario**

1. **Event:** Payment for the gift by the Giver → **Notification #1:** "Presentee’s notification of a new gift."
2. **Event:** Filling out contact information by the Presentee → **Notification #2:** "The Giver's notification of the gift confirmation. Filling out the contact information by the Presentee."
3. **Event:** Change of order status to "Completed" → **Notification #3:** "Notifying the Giver that the gift was delivered to the Presentee."
4. **Event:** Change of order status to "Not completed" → **Notification #4:** "Notifying the Giver that the gift was not delivered because of the Presentee."
5. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **Notification #5:** "Notifying the Giver that the Presentee did not confirm (ignored) accepting of the gift."
6. **Event:** Payment by the gift Giver. In case the Presentee has never previously confirmed the receipt of the gift. → **Notification #7:** "Notifying the Presentee. "Why did I get this link."