## Notification №6 "Notifying the Giver that the Presentee has agreed to receive the gift."

**Example of the data structure in JSON that may be sent to the WebHook URL of the developer**

### Notification body
```json
  {
    "create_time": "2000-01-01 00:00:00 UTC",
    "event_type": "gift_agree",
    "resource_type": "notification",
    "resource": {
      "for": "uniq_giver_id",
      "title": "short description",
      "message": "full description:\\nMake and receive real gifts with YouGiver!",
      "giver_name": "giver name",
      "recipient_name": "recipient name",
      "gift_request": "123456789",
      "links": {
        "store": "http://store.service.yougiver.me?number=12345678"
      }
    }
  }
```
